<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use Log;
use Image;

class SignUpController extends Controller
{
  private $localToken ="1";
  private $userid;
  private   $user;
  private $hasData = false;



  function isValidFacebook($fbId,$fbToken){
    $url = "https://graph.facebook.com/me?fields=id&access_token=".$fbToken;
    //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);
    $obj = json_decode($result);
    if(!isset($obj->id))
    throw new HttpException(401);
    $url= 'http://graph.facebook.com/'.$fbId.'/picture?type=square&width=640';
    $tempUser = User::where('fb_id','=',$fbId)->first();
    if($tempUser == null)
    \Image::make($url)->save(public_path('uploads/' . $fbId.'.png'));

    return ($obj->id == $fbId);


    return true;
  }


  public function logout(TokenRequest $request, JWTAuth $JWTAuth){
    if($user = \JWTAuth::authenticate($request->input('token'))){
      $user->push_token = '';
    }
    \JWTAuth::invalidate($request->input('token'));
    return response()->json([
        'status' => 'success'
    ], 200);
  }
  public function updateProfile(TokenRequest $request, JWTAuth $JWTAuth){
  // $image = null;
    if($user = \JWTAuth::authenticate($request->input('token'))){
      if($request->image){
          //$filename = substr( md5( $student->id . '-' . time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
          $filename = $user->fb_id.'.png'; // for now just assume .jpg : \
         //  $result = File::makeDirectory('alumni3');
          $path = public_path('uploads/' . $filename);
          $image = \Image::make($request->image)->orientate()->fit(500)->save($path);
          if($image)
          $user->image_url ='http://128.199.101.233/uploads/' . $user->fb_id.'.png';

        }

      $user->fill($request->all());
      $user->save();
      return response()->json([
          'status' => 'success'
      ], 200);
    }
    else{
      throw new HttpException(401);
    }
  }
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {


        if(!$this->isValidFacebook($request->input('fb_id'),$request->input('fb_token')))
            throw new HttpException(401);
            $rules = array(
            'users' => 'unique:email'
);


        try {
          // $request->request->add(['password' => $request->input('fb_id')]);

          $user = new User($request->all());
          $user->image_url ='http://128.199.101.233/uploads/' . $request->input('fb_id').'.png';
          $user->save();

    // Closures include ->first(), ->get(), ->pluck(), etc.
      }catch(\Illuminate\Database\QueryException $ex){
      if($ex->getCode() == 23000){
        //login

        if($this->login($request->input('email'),$request->input('password'),$JWTAuth,$request->input('push_token')))
      {

        return response()->json([
            'status' => 'success',
            'id' => $this->userid,
            'token' => $this->localToken,
            'profileCompleted' => $this->hasData
        ], 200);

      }else{
        throw new HttpException( 401);

      }


  // Note any method of class PDOException can be called on $ex.
      }


    }
    if(!Config::get('boilerplate.sign_up.release_token')) {


    $token = \JWTAuth::fromUser($user);
    return response()->json([
        'status' => 'Registered Successfully',
        'id' => $user->id,
        'token' => $token
    ], 201);
  }
  }

function login($email,$password,$JWTAuth,$push_token){
  $credentials = ['email'=>$email,'password'=>$password];
  $user2=User::where('email','=',$email)->first();
  if (!$userToken=\JWTAuth::fromUser($user2)) {
            return false;
        }
        $this->localToken = $userToken;
        $this->userid = $user2->id;
        if($push_token){
          $user2->push_token = $push_token;

      }
        if( $user2->nationality !=null && $user2->bdate !=null)
          $this->hasData = true;

          $user2->image_url ='http://128.199.101.233/uploads/' . $user2->fb_id.'.png';
          $user2->save();

        return true;
//
//   try {
//       $token = $JWTAuth->attempt($credentials);
//       if(!$token) {
//         return false;
//       }
//
//   } catch (JWTException $e) {
//       return false;
//   }
//     $this->localToken = $token;
//   return true;
// }

}

public function logFacebook(Request $request){

  Log:info($request->fb_body);
}

}
