<?php

namespace App\Api\V1\Controllers;

use Config;
use App\Conversation;
use App\Notification;
use App\PushHelper;
use App\User;
use App\Trip;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Image;
class ConversationController extends Controller
{

  public function getConversationsAndRequests(TokenRequest $request, JWTAuth $JWTAuth){
    \Log::info('#result#');
  if($user = \JWTAuth::authenticate($request->input('token'))){
    $conversations = DB::select('SELECT `users`.`id` as user_id, `users`.`name`, `users`.`email`,
       `users`.`fb_id`, `users`.`fb_token`, `users`.`bio`, `users`.`nationality`,
        `users`.`phone_number`, `users`.`bdate`, `users`.`rego`, `users`.`license`,
         `users`.`gmail`, `users`.`image_url`, `users`.`avg_reviews`,
          `conversations`.`user_id1`,`conversations`.`id` as conversationid ,`conversations`.`last_message`,`conversations`.`type`,`conversations`.`updated_at`
          FROM `users`, `conversations`
          WHERE   ((`conversations`.`user_id2` = `users`.`id` AND `conversations`.`user_id1` = ?)

          OR (`conversations`.`user_id1` = `users`.`id` and `conversations`.`user_id2` =  ? ))
          order by updated_at asc
         ',[$user->id, $user->id]);


          $requests = DB::select('SELECT `trips`.`trip_from`,`trips`.`trip_to`,`users`.`id` as user_id,`users`.`name`, `users`.`email`, `users`.`fb_id`, `users`.`fb_token`, `users`.`bio`, `users`.`nationality`, `users`.`phone_number`, `trips`.`date_time`, `users`.`rego`, `users`.`license`, `users`.`gmail`, `users`.`image_url`, `users`.`avg_reviews`, `trips`.`user_poster`,`trips`.`id` as trip_id ,`trips`.`type`, `trip_users`.`updated_at`'.
          ' FROM `trips`,`users`,`trip_users`'.
          ' WHERE `trip_users`.`user_id` = `users`.`id`'.' AND `trips`.`user_poster` = ? '.
          ' AND `trips`.`id` = `trip_users`.`trip_id`'.
          'AND `trip_users`.`is_poster` = 0 '.
          'AND `trip_users`.`status` = 0
           AND datediff(CURRENT_TIMESTAMP,`trips`.`date_time`) < 2
           order by updated_at asc',[$user->id]);
          // $conversations->type= 1;
          $result = array_merge($conversations,$requests);

          $myDateSort = function($obj1, $obj2) {
            $date1 = strtotime($obj1->updated_at);
            $date2 = strtotime($obj2->updated_at);
            return  $date2 - $date1; // if date1 is earlier, this will be negative
          };
            usort($result, $myDateSort);


          return response()->json( $result
    , 200);
  }
  else {
  throw new HttpException(401);

  }

  }
    public function addConversation(TokenRequest $request, JWTAuth $JWTAuth){
      $result = null;
      if($user = \JWTAuth::authenticate($request->input('token'))){
      try{
        if($request->input('user_id2')< $request->input('user_id1'))
          $result = DB::select('SELECT * FROM conversations WHERE user_id1 = ? and user_id2 = ?' , [$request->input('user_id2'),$request->input('user_id1')]);
          else
            $result = DB::select('SELECT * FROM conversations WHERE user_id1 = ? and user_id2 = ?' , [$request->input('user_id1'),$request->input('user_id2')]);

      if(count($result)>0){
        return response()->json([

            'error' => 'Conversation already exists'
        ], 400);
      }
      $conversation = new Conversation();
      $conversation->fill($request->all());
      $conversation->save();


      return response()->json([
          'status' => 'success'
      ], 200);
    }catch(\Illuminate\Database\QueryException $ex){
      if($ex->getCode() == 23000)
      return response()->json([

          'error' => 'Conversation already exists'
      ], 400);
      else
      return response()->json([
          'error' => 'unexpected error'
      ], 400);

    }
  }else{
    throw new HttpException(401);

  }
    }

    public function sendMessage(TokenRequest $request, JWTAuth $JWTAuth){
      \Log::info('#request');
      \Log::info($request);
      // $result = array();
      $result = array();
      if($user = \JWTAuth::authenticate($request->input('token'))){
        if($request->input('user_id') < $user->id){
          $result = Conversation::whereRaw('user_id1 = ? and user_id2 = ?' , [$request->input('user_id'),$user->id])->get();
          $result[0]->last_message =   $request->input('message');
          $result[0]->save();
        }else{
            $result = Conversation::whereRaw('user_id1 = ? and user_id2 = ?' , [$user->id,$request->input('user_id')])->get();
            \Log::info('SELECT * FROM conversations WHERE user_id1 = '.$user->id.' and user_id2 = '.$request->input('user_id').'###');
            $result[0]->last_message =   $request->input('message');
            $result[0]->save();
        }
        \Log::info($result.'#result#');

        $tempUser = User::whereRaw(' id = ? limit 2',[$request->input('user_id')])->get();
        $token = $tempUser[0]->push_token;
        if($token){
        $push  = new PushHelper();
        $push->sendPush($token,$user->name.': '.$request->input('message'),['user_id' =>$request->input('user_id'),'code'=>'new_message','message_id'=>$result[0]->id]);
        // $notification = new Notification();
        // $notification->user_id = $request->input('user_id');
        // $notification->trip_id = null;
        // $notification->type = 'new_message';
        // $notification->text = 'You\'ve got a new message';
        // $notification->save();
        }
}


}
}
