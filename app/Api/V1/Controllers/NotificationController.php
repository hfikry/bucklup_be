<?php

namespace App\Api\V1\Controllers;

use Config;
use App\Notification;
use App\PushHelper;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Image;
class NotificationController extends Controller
{

  public function getNotifications(TokenRequest $request, JWTAuth $JWTAuth){
  if($user = \JWTAuth::authenticate($request->input('token'))){
    $notifications = Notification::whereRaw('user_id = ? order by created_at desc limit 10',[$user->id])->get();
    return response()->json($notifications
    , 200);
  }
  else {
    throw new HttpException(401);

  }

  }
  public function addNotification(TokenRequest $request, JWTAuth $JWTAuth){
    if($user = \JWTAuth::authenticate($request->input('token'))){
      $notification = new Notification();
      $notification->user_id = $request->input('user_id2');
      $notification->type = 'new_message';
      $notification->text = 'You\'ve got a new message';
      $notification->save();
      $tempUser = User::whereRaw('where id = ? limit 2',[$request->input('user_id2')])->get();
      $token = $tempUser[0]->push_token;
      if($token){
      $push  = new PushHelper();
      $push->sendPush(1,$token,'new_message','You\'ve got a new message');
      }
    }
  }

}
