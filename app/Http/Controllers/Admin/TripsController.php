<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Trips;
use App\Http\Requests\CreateTripsRequest;
use App\Http\Requests\UpdateTripsRequest;
use Illuminate\Http\Request;



class TripsController extends Controller {

	/**
	 * Display a listing of trips
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $trips = Trips::all();

		return view('admin.trips.index', compact('trips'));
	}

	/**
	 * Show the form for creating a new trips
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.trips.create');
	}

	/**
	 * Store a newly created trips in storage.
	 *
     * @param CreateTripsRequest|Request $request
	 */
	public function store(CreateTripsRequest $request)
	{
	    
		Trips::create($request->all());

		return redirect()->route(config('quickadmin.route').'.trips.index');
	}

	/**
	 * Show the form for editing the specified trips.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$trips = Trips::find($id);
	    
	    
		return view('admin.trips.edit', compact('trips'));
	}

	/**
	 * Update the specified trips in storage.
     * @param UpdateTripsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTripsRequest $request)
	{
		$trips = Trips::findOrFail($id);

        

		$trips->update($request->all());

		return redirect()->route(config('quickadmin.route').'.trips.index');
	}

	/**
	 * Remove the specified trips from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Trips::destroy($id);

		return redirect()->route(config('quickadmin.route').'.trips.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Trips::destroy($toDelete);
        } else {
            Trips::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.trips.index');
    }

}
