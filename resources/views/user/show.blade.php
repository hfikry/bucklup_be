@extends('vendor.crud.single-page-templates.common.app')

@section('content')

    <h2>User: {{$user->role_id}}</h2>

    <ul class="list-group">

        <li class="list-group-item">
            <h4>Id</h4>
            <h5>{{$user->id}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Role Id</h4>
            <h5>{{$user->role_id}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Name</h4>
            <h5>{{$user->name}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Email</h4>
            <h5>{{$user->email}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Fb Id</h4>
            <h5>{{$user->fb_id}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Fb Token</h4>
            <h5>{{$user->fb_token}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Bio</h4>
            <h5>{{$user->bio}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Nationality</h4>
            <h5>{{$user->nationality}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Phone Number</h4>
            <h5>{{$user->phone_number}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Bdate</h4>
            <h5>{{$user->bdate}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Rego</h4>
            <h5>{{$user->rego}}</h5>
        </li>
        <li class="list-group-item">
            <h4>License</h4>
            <h5>{{$user->license}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Paypal Email</h4>
            <h5>{{$user->paypal_email}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Gmail</h4>
            <h5>{{$user->gmail}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Image Url</h4>
            <h5>{{$user->image_url}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Avg Reviews</h4>
            <h5>{{$user->avg_reviews}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Password</h4>
            <h5>{{$user->password}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Device Type</h4>
            <h5>{{$user->device_type}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Push Token</h4>
            <h5>{{$user->push_token}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Remember Token</h4>
            <h5>{{$user->remember_token}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Created At</h4>
            <h5>{{$user->created_at}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Updated At</h4>
            <h5>{{$user->updated_at}}</h5>
        </li>
        <li class="list-group-item">
            <h4>Active</h4>
            <h5>{{$user->active}}</h5>
        </li>
        
    </ul>

@endsection