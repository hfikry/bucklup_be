<?php

namespace App\Api\V1\Controllers;

use Config;
use App\Destination;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

// use Illuminate\Support\Facades\DB;

use Image;
class ConfigController extends Controller
{

  public function getConfig(TokenRequest $request, JWTAuth $JWTAuth){
    if($user = \JWTAuth::authenticate($request->input('token'))){

    $versionNumber = $request->input('version_number');
    $deviceType = $request->input('device_type');
    $userId = $user->id;
    $shouldReview = false;

    // $queryResult = DB::select('SELECT  `trip_users`.`trip_id` as trip_id,`trip_users`.`user_id`  as user_id,`users`.`name`,`trips`.`trip_from`,`trips`.`trip_to`,`trips`.`date_time`
    //                             FROM `trips`,`users` , `trip_users`
    //                             WHERE datediff(`date_time`,CURRENT_DATE) < 0
    //                             AND `trip_users`.`trip_id` = `trips`.`id`
    //                             AND `trips`.`id` = `trip_users`.`trip_id`
    //                             AND `users`.`id` = `trip_users`.`user_id`
    //                             AND `trip_users`.`user_id` = ?
    //                             AND NOT EXISTS ( SELECT `trip_id`
    //                                              FROM `user_reviews`
    //                                              WHERE `user_reviews`.`trip_id` = `trips`.`id`
    //                                              AND `user_reviews`.`user_receiver`= `users`.`id`
    //                                              AND `user_reviews`.`user_writer` = ?)
    //                                              ORDER BY `trips`.`id`',[$userId,$userId]);

$queryResult = DB::table('future_reviews')
          ->join('trips', 'future_reviews.trip_id', '=', 'trips.id')
          ->join('users', 'future_reviews.user_receiver', '=', 'users.id')
          ->select('future_reviews.*','users.*','trips.*')
          ->where('future_reviews.user_writer', '=', $userId)
          ->whereRaw('datediff(trips.date_time,CURRENT_TIMESTAMP) < -1')
          ->get();
    if(count($queryResult)>0)
    {
      $shouldReview = true;
      return response()->json([
          'forceUpdate' => false,
          'storeLink' => 'store Link',
          'shouldReview'=>$shouldReview,
          'review'=>$queryResult


      ], 200);
    }
    else{
      $shouldReview = false;
      return response()->json([
          'forceUpdate' => false,
          'storeLink' => 'store Link',
          'shouldReview'=>$shouldReview


      ], 200);
    }

}

  }

}
