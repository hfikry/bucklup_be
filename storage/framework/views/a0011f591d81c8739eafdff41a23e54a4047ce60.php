<div class="panel-group col-md-6 col-sm-12" id="accordion" style="padding-left: 0">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <i class="fa fa-plus"></i>
                    Add a New User                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">

                <form action="/user" method="post">

                    <?php echo e(csrf_field()); ?>


                    <?php echo \Nvd\Crud\Form::input('role_id','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('name','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('email','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('fb_id','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('fb_token','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('bio','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('nationality','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('phone_number','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('bdate','date')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('rego','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('license','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::textarea( 'paypal_email' )->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('gmail','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('image_url','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('avg_reviews','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::textarea( 'password' )->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('device_type','text')->show(); ?>


                    <?php echo \Nvd\Crud\Form::textarea( 'push_token' )->show(); ?>


                    <?php echo \Nvd\Crud\Form::textarea( 'remember_token' )->show(); ?>


                    <?php echo \Nvd\Crud\Form::input('active','text')->show(); ?>


                    <button type="submit" class="btn btn-primary">Create</button>

                </form>

            </div>
        </div>
    </div>
</div>