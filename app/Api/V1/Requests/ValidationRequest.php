<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class ValidationRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.add_validation.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
