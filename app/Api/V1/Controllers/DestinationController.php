<?php

namespace App\Api\V1\Controllers;

use Config;
use App\Destination;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

// use Illuminate\Support\Facades\DB;

use Image;
class DestinationController extends Controller
{

  public function getDestinations(TokenRequest $request, JWTAuth $JWTAuth){
  if($user = \JWTAuth::authenticate($request->input('token'))){
    $pricePerKm = DB::select('select * from price_per_km');
    $destinations = Destination::all();
    $result = new \stdClass();
    $result -> price = $pricePerKm[0]->price;
    $result->destinations = $destinations;

    return response()->json(
    $result
    , 200);
  }
  else {
    throw new HttpException(401);

  }

  }
  public function postDestinations(Request $request ){
if (($handle = fopen("/var/www/html/dropseats/app/Api/V1/Controllers/citiesall.csv", "r")) !== FALSE) {
  $i = 0;
  while (($data = fgetcsv($handle, 1000, ",")) != FALSE) {

    if($i !=0 && $data[2]!= NULL  && $data!=''){
        echo $data[2] . " ".$data[7] . " ".$data[8] . " <br>";
        $destination = new Destination();
        $destination->destination = $data[2];
        $destination->latitude = $data[7];
        $destination->longitude = $data[8];
        try{
        $destination->save();
      }catch(\Illuminate\Database\QueryException $ex){
      }

      }
      $i++;
  }
  fclose($handle);
}
}
public function postTowns(Request $request ){
if (($handle = fopen("/var/www/html/dropseats/app/Api/V1/Controllers/extracities.csv", "r")) !== FALSE) {
$i = 0;
while (($data = fgetcsv($handle, 1000, ",")) != FALSE) {


      $destination = new Destination();
      $destination->destination = $data[0];
      $destination->latitude = $data[2];
      $destination->longitude = $data[3];
      try{
      $destination->save();
    }catch(\Illuminate\Database\QueryException $ex){
    }


    $i++;
}
fclose($handle);
}
}
}
