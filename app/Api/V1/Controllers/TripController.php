<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use App\PaymentHelper;
use App\TripUser;
use App\Notification;
use App\PushHelper;
use App\Trip;
use App\UserReview;
use App\FutureReview;

use App\Http\Controllers\Controller;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use GuzzleHttp\Client;
use Image;
use PayPal\Auth\OAuthTokenCredential;

class TripController extends Controller
{


    public function postATrip(TokenRequest $request, \JWTAuth $JWTAuth)
    {
      if($user = \JWTAuth::authenticate($request->input('token'))){
        $trip = new Trip();
        $trip->fill($request->all());
        try {
        // $request->request->add(['total_seats' => $request->input('seats')]);
        $trip->price_per_seat = strval(intval($trip->price_per_seat*1.05));
        $saved = $trip->save();
        $tripUser = new TripUser();
        $tripUser->trip_id = $trip->id;
        $tripUser->user_id = $user->id;
        $tripUser->is_poster = 1;
        $tripUser->save();
        return response()->json([
            'status' => 'success',
            'code' => 200

        ], 200);
      }catch(\Illuminate\Database\QueryException $ex){
          return response()->json([
              'error' => 'Error posting trip',
              'code' => 500

          ], 400);
          }
      }
      else {
        return response()->json([
            'error' => 'invalid token'
        ], 400);
      }
    }
    public function cancelATrip(TokenRequest $request, \JWTAuth $JWTAuth)
    {
      if($user = \JWTAuth::authenticate($request->input('token'))){

        try {
        // $request->request->add(['total_seats' => $request->input('seats')]);

        $tripUser = TripUser::select('*')
        ->where('trip_users.trip_id','=',$request->input('trip_id'))
        ->where('trip_users.user_id','=',$user->id)
        ->delete();
        $trip = Trip::find($request->input('trip_id'));
        $seats = $trip->seats;
        $seats = $seats -1;

        $totalSeats = $trip->total_seats;
        $totalSeats = $totalSeats +1;

        if($user->id == $trip->user_poster)
          $trip->delete();
        else
          $trip->save();
        return response()->json([
            'status' => 'success',
            'code' => 200

        ], 200);
      }catch(\Illuminate\Database\QueryException $ex){
          return response()->json([
              'error' => 'Error deleting trip',
              'code' => 500

          ], 400);
          }
      }
      else {
        return response()->json([
            'error' => 'invalid token'
        ], 400);
      }
    }
    public function searchForATrip(TokenRequest $request, \JWTAuth $JWTAuth)
    {
      if($user = \JWTAuth::authenticate($request->input('token'))){
        $tripsExactDate = DB::select('select `trips`.*, `users`.`name`, `users`.`image_url`, `users`.`avg_reviews`
            from `trips`,`users`,`trip_users`
            where `users`.`id` = `trips`.`user_poster`
            and trip_from = ? and trip_to = ?
            and `trip_users`.`trip_id` = `trips`.`id`
            and `trip_users`.`user_id` = `users`.`id`
            and seats > 0
            and datediff( ? ,`trips`.`date_time`)=0
            order by `trips`.`created_at` desc', [$request->input('from'),$request->input('to'),strval(($request->input('date_time'))),$user->id]);

          $trips = DB::select('select `trips`.*, `users`.`name`, `users`.`image_url`, `users`.`avg_reviews`
              from `trips`,`users`,`trip_users`
              where `users`.`id` = `trips`.`user_poster`
              and trip_from = ? and trip_to = ?
              and `trip_users`.`trip_id` = `trips`.`id`
              and `trip_users`.`user_id` = `users`.`id`
              and seats > 0
              and datediff( ? ,`trips`.`date_time`) !=0
              and date_time >= CURRENT_DATE
              order by `trips`.`date_time`', [$request->input('from'),$request->input('to'),strval($request->input('date_time')),$user->id]);
          // $trips2 = Trip::whereRaw('trip_from = ? and trip_to = ? and date_time > CURRENT_TIME', [$request->input('to'),$request->input('from')])->get();
          // $result = ->merge();
            $result = $tripsExactDate + $trips  ;
          return response()->json(
            $result
          , 200);

      }  else {
          return response()->json([
              'error' => 'invalid token'
          ], 400);
        }
}
public function requestToJoinATrip(TokenRequest $request, \JWTAuth $JWTAuth)
{
  if($user = \JWTAuth::authenticate($request->input('token'))){
      if($user->gmail && $user->license ){
    try{
      $tempTrip = Trip::whereRaw('id = ? ', [$request->input('trip_id')])->get();

      if($tempTrip[0]->seats <= 0){
        return response()->json([
            'error' => 'No seats available in this trip'
        ], 400);

      }

      $tripUser = new TripUser();
      $tripUser->trip_id = $tempTrip[0]->id;
      $tripUser->user_id =$user->id;
      // $tripUser->updated_at = CURRENT_TIMESTAMP;
      $tripUser->save();
      $notification = new Notification();
      $notification->user_id = $tempTrip[0]->user_poster;
      $notification->trip_id = $tempTrip[0]->id;
      $notification->type = 'new_request';
      $notification->text = 'You\'ve got a new request';

      $notification->save();
      if(!empty($request->input('mdid'))){
        $user->mdid = $request->input('mdid');
        $user->auth_code = $request->input('auth_code');
        $user->save();
      }


      $tempUser = User::whereRaw(' id = ? limit 2',[$tempTrip[0]->user_poster])->get();
      $token = $tempUser[0]->push_token;
      if($token){
      $push  = new PushHelper();
      $push->sendPush($token,'You received a new Request',['code'=>'new_request']);
      }
      return response()->json([
          'success' => 'ok'
      ], 200);

      }catch(\Illuminate\Database\QueryException $ex){
        return response()->json([
          'code' => 11,
            'status' => 'error',
            'message' => 'You already requested to join'
        ], 406);
      }
    }else{
      return response()->json([
          'code' => 12,
          'status' => 'error',
          'message' => 'Verifications not completed'
      ], 406);
    }
    }else {
        throw new HttpException(401);

      }
      }
      public function acceptTripRequest(TokenRequest $request, \JWTAuth $JWTAuth)
      {
        if($user = \JWTAuth::authenticate($request->input('token'))){
          try{
            $tempTrip = Trip::whereRaw('id = ? ', [$request->input('trip_id')])->get();

            $seats = $tempTrip[0]->seats;
            $seats = $seats -1;

            $totalSeats = $tempTrip[0]->total_seats;
            $totalSeats = $totalSeats +1;


            if($seats<0)
            return response()->json([
                'status' => 'error',
                'message' =>'Sorry, there are no seats left'
            ], 406);

            $otherUserId = $request->input('user_id');
            $tempUser = User::whereRaw(' id = ? limit 2',[$otherUserId])->get();
            $token = $tempUser[0]->push_token;
            if($token){
            $push  = new PushHelper();
            $push->sendPush($token,'Congratulations '.$tempUser[0]->name.', your request has been accepted',['code'=>'accepted_request']);
            $notification = new Notification();
            $notification->user_id = $otherUserId;
            $notification->type = 'accepted_request';
            $notification->text = 'Congratulations, your request has been accepted!';
            $notification->save();

            $deleteNotification  = Notification::whereRaw('trip_id = ? and user_id = ? ',[$tempTrip[0]->id, $user->id ])->get();
            $deleteNotification[0]->delete();

            }
            if($otherUserId == null)
            return response()->json([
                'message' => 'please specify user_id'
            ], 406);
              $tripUser =TripUser::whereRaw('user_id = ? and trip_id = ?',[$request->input('user_id'),$tempTrip[0]->id])->get();

              $tripUser[0]->status = 1 ;
              $tripUser[0]->save();



            $tempTrip[0]->seats = $seats;
            $tempTrip[0]->total_seats = $totalSeats;
            $tempTrip[0]->save();



          // $trip = DB::table('trips')
          //     ->where('id',$request->input('trip_id') )  // find your user by their email
          //     ->limit(1)  // optional - to ensure only one record is updated.
          //     ->update(array('user_joiner' => $request->input('user_id'),'status'=> $request->status));
          $payment = new PaymentHelper();
          // $payment->deductMoneyUsingMDID($otherUserId,$tempTrip[0]->price_per_seat,$auth_code);
          $payment->PaypalFuturePayment($otherUserId,$tempTrip[0]->price_per_seat);
          $futureReview = new FutureReview();
          $futureReview->user_writer = $user->id;
          $futureReview->user_receiver = $request->input('user_id');
          $futureReview->trip_id = $tempTrip[0]->id;
          $futureReview->save();

          $futureReview = new FutureReview();
          $futureReview->user_writer = $request->input('user_id');
          $futureReview->user_receiver = $user->id;
          $futureReview->trip_id = $tempTrip[0]->id;
          $futureReview->save();
              return response()->json([
                  'success' => 'ok'
              ], 200);


            }catch(\Illuminate\Database\QueryException $ex){
              return response()->json([
                  'message' => 'could send the request'.$ex
              ], 406);
            }
        }else{
          throw new HttpException(401);
        }
      }
      public function rejectTripRequest(TokenRequest $request, \JWTAuth $JWTAuth)
      {
        if($user = \JWTAuth::authenticate($request->input('token'))){
          try{
            $tempTrip = Trip::whereRaw('id = ? ', [$request->input('trip_id')])->get();
              $tripUser =TripUser::whereRaw('user_id = ? and trip_id = ?',[$request->input('user_id'),$tempTrip[0]->id])->get();
              $tripUser[0]->delete();
              $deleteNotification  = Notification::whereRaw('trip_id = ? and user_id = ? ', [$tempTrip[0]->id, $user->id ])->get();
              $deleteNotification[0]->delete();
              return response()->json([
                  'success' => 'ok'
              ], 200);


            }catch(\Illuminate\Database\QueryException $ex){
              return response()->json([
                  'error' => 'could send the request'
              ], 406);
            }
          }else {
            throw new HttpException(401);

            }
            }


            public function myTrips(TokenRequest $request, \JWTAuth $JWTAuth)
            {
              if($user = \JWTAuth::authenticate($request->input('token'))){
                try{
                  $tempTrip = DB::table('trip_users')
                        ->select('trips.*','user2.image_url','user2.name','dest.destination as trip_from','dest2.destination as trip_to')
                        ->join('trips','trip_users.trip_id','=','trips.id')
                        ->join('users as user1','trip_users.user_id','=','user1.id')
                        ->join('users as user2','trips.user_poster','=','user2.id')
                        ->join('destinations as dest','trips.trip_from','=','dest.id')
                        ->join('destinations as dest2','trips.trip_to','=','dest2.id')
                        // ->whereRaw('datediff(trips.date_time,CURRENT_TIMESTAMP) > 0')
                        ->whereRaw('user1.id = ? OR user2.id = ?', [$user->id, $user->id])
                        // ->whereOr('user2.id', '=', $user->id)
                        ->orderBy('trips.date_time','desc')
                        // ->where('trip_users.is_poster','=','0')
                        // ->whereOr('trip_users')
                        ->get();
                  // $tempTrip2 = DB::table('trip_users')
                  //       ->select('trips.*','users.*')
                  //       ->join('users','trips.user_receiver','=','users.id')
                  //       ->whereOr('trips.user_receiver', '=', $user->id)
                  //       ->get();
                  // $result = $tempTrip->merge($tempTrip2);

                    return response()->json(
                        $tempTrip
                    , 200);


                  }catch(\Illuminate\Database\QueryException $ex){
                    return response()->json([
                        'message' => 'couldn\'t get trips'
                    ], 406);
                  }
                }else {
                  throw new HttpException(401);

                  }
                  }


public function tripsCronJob(){
  $queryResult = DB::select('SELECT `push_token` as push_token,name, `trip_users`.`trip_id` as trip_id,`users`.`id` as user_id,`trips`.`trip_from`,`trips`.`trip_to`
                              FROM `trips`,`users` , `trip_users`
                              WHERE datediff(`date_time`,CURRENT_DATE) = -2
                              AND `trip_users`.`trip_id` = `trips`.`id`
                              And `trip_users`.`user_id` = `users`.`id`');
  $pushHelper = new pushHelper();

  foreach ($queryResult as $value) {
    //send push to passenger
    if($value->push_token != '')
      $pushHelper->sendPush($value->push_token,'Please review your last ride with Buckle Up!',['code'=>'review','user_id'=>$value->user_id,'trip_from'=>$value->trip_from,'trip_to'=>$value->trip_to]);

  }

  $queryPayments = DB::select('SELECT `trips`.`id` as trip_id,`users`.`paypal_email`,`trips`.`total_seats`,`trips`.`price_per_seat`,`users`.`push_token`,`users`.`id` as user_id
                                FROM `trips`,`users`,`trip_users`
                                Where `users`.`id` = `trip_users`.`user_id`
                                and `trips`.`id` = `trip_users`.`trip_id`
                                and `trip_users`.`is_poster` = 1
                                and  datediff(`trips`.`date_time`,CURRENT_DATE) = -2
                                and `trips`.`is_paid` = 0'
                              );
  $paymentUrl='https://api-3t.sandbox.paypal.com/nvp?USER=hossam.alaa.fikry-facilitator-2_api1.gmail.com&PWD=W8QF7XAP79PYJLFS&SIGNATURE=AFcWxV21C7fd0v3bYYYRCpSSRl31Axx139Nj.fk2nW5NpyGpZLIecjOK&VERSION=204&METHOD=MassPay&RECEIVERTYPE=EmailAddress&CURRENCYCODE=AUD';
  $shouldPay = false;
  //masspay
  for ($i=0; $i<count($queryPayments); $i++) {
      $shouldPay = true;
      //PayPal url
      $value = intval($queryPayments[$i]->price_per_seat)*0.95*intval($queryPayments[$i]->total_seats);
      $value = $value *0.95;

      $paymentUrl = $paymentUrl.'&L_EMAIL'.strval($i).'='.$queryPayments[$i]->paypal_email.'&L_AMT'.strval($i).'='.''.strval($value);

// $value =
      //calculating fare
    // $pushHelper = new pushHelper();
    // $pushHelper->sendPush($queryPayments[$i]->push_token,'Congratulations, you\'ve received a new payment!',['code'=>'new_payment']);
    // adding to notification table
    $notification = new Notification();
    $notification->user_id = $queryPayments[$i]->user_id;
    $notification->type = 'new_payment';
    $notification->text = 'Congratulations, you\'ve received a new payment!';
    $notification->save();

    $updatableTrip = Trip::whereId(intval($queryPayments[$i]->trip_id))->first();
    $updatableTrip->is_paid = 1;
    $updatableTrip->save();

}

// $response = file_get_contents($paymentUrl);
if($shouldPay == true){
    $client = new Client(['defaults' => ['verify' => false]]);
    // $response = $client->request('GET',$paymentUrl);
}
    // return $response->id;;
return response()->json([
    'status' =>  $paymentUrl
], 200);
}

public function Seb(){
  $token = "dEcKLM-GFzI:APA91bFHBRMbje9XtyaHf0iHJp-W3omqQT7wZRpo6sxHUD0a831OAU-heVpMxMXvEjfbLAsKgDn05ELFceP1ORYtuazdlZVXDzzBBkk1RhTpIGP2rh0jzbfH6tMlnrWj0gL1C41Ppvi7";
  $push  = new PushHelper();
  $push->sendPush($token,'You received a new Request',['code'=>'new_request']);
}
public function FuturePaymentTest(TokenRequest $request, \JWTAuth $JWTAuth){
  $payment = new PaymentHelper();
  return response()->json([
      'message' => $payment->PaypalFuturePayment(356,40)
  ], 406);

}

}
