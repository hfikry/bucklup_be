@extends('vendor.crud.single-page-templates.common.app')

@section('content')

	<h2>Users</h2>

	@include('user.create')

	<table class="table table-striped grid-view-tbl">
	    
	    <thead>
		<tr class="header-row">
			{!!\Nvd\Crud\Html::sortableTh('id','user.index','Id')!!}
			{!!\Nvd\Crud\Html::sortableTh('role_id','user.index','Role Id')!!}
			{!!\Nvd\Crud\Html::sortableTh('name','user.index','Name')!!}
			{!!\Nvd\Crud\Html::sortableTh('email','user.index','Email')!!}
			{!!\Nvd\Crud\Html::sortableTh('fb_id','user.index','Fb Id')!!}
			{!!\Nvd\Crud\Html::sortableTh('fb_token','user.index','Fb Token')!!}
			{!!\Nvd\Crud\Html::sortableTh('bio','user.index','Bio')!!}
			{!!\Nvd\Crud\Html::sortableTh('nationality','user.index','Nationality')!!}
			{!!\Nvd\Crud\Html::sortableTh('phone_number','user.index','Phone Number')!!}
			{!!\Nvd\Crud\Html::sortableTh('bdate','user.index','Bdate')!!}
			{!!\Nvd\Crud\Html::sortableTh('rego','user.index','Rego')!!}
			{!!\Nvd\Crud\Html::sortableTh('license','user.index','License')!!}
			{!!\Nvd\Crud\Html::sortableTh('paypal_email','user.index','Paypal Email')!!}
			{!!\Nvd\Crud\Html::sortableTh('gmail','user.index','Gmail')!!}
			{!!\Nvd\Crud\Html::sortableTh('image_url','user.index','Image Url')!!}
			{!!\Nvd\Crud\Html::sortableTh('avg_reviews','user.index','Avg Reviews')!!}
			{!!\Nvd\Crud\Html::sortableTh('password','user.index','Password')!!}
			{!!\Nvd\Crud\Html::sortableTh('device_type','user.index','Device Type')!!}
			{!!\Nvd\Crud\Html::sortableTh('push_token','user.index','Push Token')!!}
			{!!\Nvd\Crud\Html::sortableTh('remember_token','user.index','Remember Token')!!}
			{!!\Nvd\Crud\Html::sortableTh('created_at','user.index','Created At')!!}
			{!!\Nvd\Crud\Html::sortableTh('updated_at','user.index','Updated At')!!}
			{!!\Nvd\Crud\Html::sortableTh('active','user.index','Active')!!}
			<th></th>
		</tr>
		<tr class="search-row">
			<form class="search-form">
				<td><input type="text" class="form-control" name="id" value="{{Request::input("id")}}"></td>
				<td><input type="text" class="form-control" name="role_id" value="{{Request::input("role_id")}}"></td>
				<td><input type="text" class="form-control" name="name" value="{{Request::input("name")}}"></td>
				<td><input type="text" class="form-control" name="email" value="{{Request::input("email")}}"></td>
				<td><input type="text" class="form-control" name="fb_id" value="{{Request::input("fb_id")}}"></td>
				<td><input type="text" class="form-control" name="fb_token" value="{{Request::input("fb_token")}}"></td>
				<td><input type="text" class="form-control" name="bio" value="{{Request::input("bio")}}"></td>
				<td><input type="text" class="form-control" name="nationality" value="{{Request::input("nationality")}}"></td>
				<td><input type="text" class="form-control" name="phone_number" value="{{Request::input("phone_number")}}"></td>
				<td><input type="date" class="form-control" name="bdate" value="{{Request::input("bdate")}}"></td>
				<td><input type="text" class="form-control" name="rego" value="{{Request::input("rego")}}"></td>
				<td><input type="text" class="form-control" name="license" value="{{Request::input("license")}}"></td>
				<td><input type="text" class="form-control" name="paypal_email" value="{{Request::input("paypal_email")}}"></td>
				<td><input type="text" class="form-control" name="gmail" value="{{Request::input("gmail")}}"></td>
				<td><input type="text" class="form-control" name="image_url" value="{{Request::input("image_url")}}"></td>
				<td><input type="text" class="form-control" name="avg_reviews" value="{{Request::input("avg_reviews")}}"></td>
				<td><input type="text" class="form-control" name="password" value="{{Request::input("password")}}"></td>
				<td><input type="text" class="form-control" name="device_type" value="{{Request::input("device_type")}}"></td>
				<td><input type="text" class="form-control" name="push_token" value="{{Request::input("push_token")}}"></td>
				<td><input type="text" class="form-control" name="remember_token" value="{{Request::input("remember_token")}}"></td>
				<td><input type="text" class="form-control" name="created_at" value="{{Request::input("created_at")}}"></td>
				<td><input type="text" class="form-control" name="updated_at" value="{{Request::input("updated_at")}}"></td>
				<td><input type="text" class="form-control" name="active" value="{{Request::input("active")}}"></td>
				<td style="min-width: 6em;">@include('vendor.crud.single-page-templates.common.search-btn')</td>
			</form>
		</tr>
	    </thead>

	    <tbody>
	    	@forelse ( $records as $record )
		    	<tr>
					<td>
						{{ $record->id }}
						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="role_id"
							  data-value="{{ $record->role_id }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->role_id }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="name"
							  data-value="{{ $record->name }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->name }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="email"
							  data-name="email"
							  data-value="{{ $record->email }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->email }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="fb_id"
							  data-value="{{ $record->fb_id }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->fb_id }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="fb_token"
							  data-value="{{ $record->fb_token }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->fb_token }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="bio"
							  data-value="{{ $record->bio }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->bio }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="nationality"
							  data-value="{{ $record->nationality }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->nationality }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="phone_number"
							  data-value="{{ $record->phone_number }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->phone_number }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="date"
							  data-name="bdate"
							  data-value="{{ $record->bdate }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->bdate }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="rego"
							  data-value="{{ $record->rego }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->rego }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="license"
							  data-value="{{ $record->license }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->license }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="paypal_email"
							  data-value="{{ $record->paypal_email }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->paypal_email }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="gmail"
							  data-value="{{ $record->gmail }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->gmail }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="image_url"
							  data-value="{{ $record->image_url }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->image_url }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="avg_reviews"
							  data-value="{{ $record->avg_reviews }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->avg_reviews }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="password"
							  data-value="{{ $record->password }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->password }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="device_type"
							  data-value="{{ $record->device_type }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->device_type }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="push_token"
							  data-value="{{ $record->push_token }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->push_token }}</span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="remember_token"
							  data-value="{{ $record->remember_token }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->remember_token }}</span>
						</td>
					<td>
						{{ $record->created_at }}
						</td>
					<td>
						{{ $record->updated_at }}
						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="active"
							  data-value="{{ $record->active }}"
							  data-pk="{{ $record->{$record->getKeyName()} }}"
							  data-url="/user/{{ $record->{$record->getKeyName()} }}"
							  >{{ $record->active }}</span>
						</td>
					@include( 'vendor.crud.single-page-templates.common.actions', [ 'url' => 'user', 'record' => $record ] )
		    	</tr>
			@empty
				@include ('vendor.crud.single-page-templates.common.not-found-tr',['colspan' => 24])
	    	@endforelse
	    </tbody>

	</table>

	@include('vendor.crud.single-page-templates.common.pagination', [ 'records' => $records ] )

<script>
	$(".editable").editable({ajaxOptions:{method:'PUT'}});
</script>
@endsection