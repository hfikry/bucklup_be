<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('fbconnect', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('logout', 'App\\Api\\V1\\Controllers\\SignUpController@logout');


        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to this item is only for authenticated user. Provide a token in your request!'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('books', 'App\\Api\\V1\\Controllers\\BookController@index');
    $api->get('books/{id}', 'App\\Api\\V1\\Controllers\\BookController@show');
    $api->post('books', 'App\\Api\\V1\\Controllers\\BookController@store');
    $api->put('books/{id}', 'App\\Api\\V1\\Controllers\\BookController@update');
    $api->delete('books/{id}', 'App\\Api\\V1\\Controllers\\BookController@destroy');
    // $api->post('user/getProfile', 'App\\Api\\V1\\Controllers\\UserController@getProfile');

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });

    $api->post('addVerification', 'App\\Api\\V1\\Controllers\\ValidationController@addValidation');
    $api->post('add_validation/license', 'App\\Api\\V1\\Controllers\\ValidationController@addLicense');
    $api->post('user/getVerifications', 'App\\Api\\V1\\Controllers\\ValidationController@getValidations');
    $api->post('user/getMyProfile', 'App\\Api\\V1\\Controllers\\UserController@getMyProfile');
    $api->post('user/getUsersProfile', 'App\\Api\\V1\\Controllers\\UserController@getUsersProfile');
    $api->post('user/review', 'App\\Api\\V1\\Controllers\\UserController@reviewAUser');
    $api->post('user/requests', 'App\\Api\\V1\\Controllers\\ConversationController@getConversationsAndRequests');
    $api->post('user/update', 'App\\Api\\V1\\Controllers\\SignUpController@updateProfile');
    $api->post('user/test', 'App\\Api\\V1\\Controllers\\UserController@testPush');
    $api->post('user/testPayment', 'App\\Api\\V1\\Controllers\\UserController@registerPaypalUser');
    $api->post('destinations','App\\Api\\V1\\Controllers\\DestinationController@getDestinations');
    $api->get('parse','App\\Api\\V1\\Controllers\\DestinationController@postDestinations');
    $api->get('parseTowns','App\\Api\\V1\\Controllers\\DestinationController@postTowns');

    $api->post('conversation/add','App\\Api\\V1\\Controllers\\ConversationController@addConversation');
    $api->post('conversation/sendMessage','App\\Api\\V1\\Controllers\\ConversationController@sendMessage');
    $api->get('testpayment','App\\Api\\V1\\Controllers\\TripController@FuturePaymentTest');


    $api->post('trip/post', 'App\\Api\\V1\\Controllers\\TripController@postATrip');
    $api->post('trip/search', 'App\\Api\\V1\\Controllers\\TripController@searchForATrip');
    $api->post('trip/request', 'App\\Api\\V1\\Controllers\\TripController@requestToJoinATrip');
    $api->post('trip/respond', 'App\\Api\\V1\\Controllers\\TripController@respondToJoinATrip');
    $api->post('trip/accept', 'App\\Api\\V1\\Controllers\\TripController@acceptTripRequest');
    $api->post('trip/reject', 'App\\Api\\V1\\Controllers\\TripController@rejectTripRequest');
    $api->post('trip/myTrips', 'App\\Api\\V1\\Controllers\\TripController@myTrips');
    $api->post('trip/cancel', 'App\\Api\\V1\\Controllers\\TripController@cancelATrip');

    $api->post('notifications', 'App\\Api\\V1\\Controllers\\NotificationController@getNotifications');
    $api->post('notifications/add', 'App\\Api\\V1\\Controllers\\NotificationController@addNotification');
    $api->get('tripsCronJob', 'App\\Api\\V1\\Controllers\\TripController@tripsCronJob');
    $api->get('seb', 'App\\Api\\V1\\Controllers\\TripController@Seb');
    $api->get('getConfig', 'App\\Api\\V1\\Controllers\\ConfigController@getConfig');
    $api->get('getSebToken', 'App\\Api\\V1\\Controllers\\UserController@getSebToken');

    $api->post('logFacebook', 'App\\Api\\V1\\Controllers\\SignUpController@logFacebook');


});
