<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Image;
class ValidationController extends Controller
{
    public function getValidations(TokenRequest $request, JWTAuth $JWTAuth){
      if($user = \JWTAuth::authenticate($request->input('token'))){
        $gmail = array('key' => 'gmail','value'=>($user->gmail!=null),'details'=>$user->gmail);
        $rego = array('key' => 'rego','value'=>($user->rego!=null),'details'=>$user->rego);
        $license = array('key'=>'license','value'=>($user->license!=null),'details'=>$user->license);
        $phoneNumber = array('key'=>'phone','value'=>($user->phone_number!=null),'details'=>$user->phone_number);
        return response()->json([
          $gmail,
          $license,
          $rego,
          $phoneNumber


        ], 200);
      }else{
          throw new HttpException(401);
      }
    }
    public function addValidation(TokenRequest $request, JWTAuth $JWTAuth)
    {
      if($user = \JWTAuth::authenticate($request->input('token'))){
    $test=  DB::table('users')
          ->where('id', $user->id)
          ->update([ $request->input('key') => $request->input('value')]);
          return response()->json([
              'status' => 'success'
          ], 200);
        }else{
            throw new HttpException(401);
        }

    }
    public function addLicense(TokenRequest $request, JWTAuth $JWTAuth)
    {

if($user = \JWTAuth::authenticate($request->input('token'))){
       // do we have an image to process?
       if($request->image){
           //$filename = substr( md5( $student->id . '-' . time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
           $filename = $user->id.'.png'; // for now just assume .jpg : \
          //  $result = File::makeDirectory('alumni3');
           $path = public_path('uploads/licenses/' . $filename);
           $image = \Image::make($request->image)->orientate()->fit(500)->save($path);
           if($image){
             DB::table('users')
                 ->where('id', $user->id)
                 ->update([ 'license' => 'http://128.199.101.233/uploads/licenses/' . $filename]);
           return response()->json([
               'status' => 'ok'
           ], 200);



         }else

           return response()->json([
               'error' => 'unexpected error'
           ], 400);
         }else{
           return response()->json([
               'error' => 'unexpected error'
           ], 400);
         }
         }
          else{
       throw new HttpException(401);
       }

}
}
