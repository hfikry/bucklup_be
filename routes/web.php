<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('loaderio-e7ab8aa352d6fd2c4712cbffe2356cb2.txt', function () {
    // return view('welcome');
    $headers = ['Content-type'=>'text/plain'];
    return response()->download(public_path()."/loaderio-e7ab8aa352d6fd2c4712cbffe2356cb2.txt", "loaderio-e7ab8aa352d6fd2c4712cbffe2356cb2.txt", $headers);

});
Route::resource('user','UserController');
