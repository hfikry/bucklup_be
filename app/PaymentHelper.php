<?php
namespace App;
use PayPal\Auth\OAuthTokenCredential;
use Paypalpayment;

 class PaymentHelper {
   private $_apiContext;

       public function __construct()
       {

           $this->_apiContext = Paypalpayment::ApiContext(config('paypal_payment.Account.ClientId'), config('paypal_payment.Account.ClientSecret'));

       }

public function getAccessToken($code){
  $curl = curl_init();
  // $code = '';
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.sandbox.paypal.com/v1/identity/openidconnect/tokenservice?grant_type=authorization_code&code=".$code,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //  CURLOPT_USERPWD => 'ARJc4HgfMcVTH4Z8fv3XW7K2lEzV3KFGe_d6qlZ_oRl17ljShJAiPqz17VuO3XsnhvUNxnhjiBiSue5v:EHzLkyHcS_Zo6Z6HarSlO1iFGJ_yIuQsiBaihrUHXtaU0knG2mdf39z1SAuix3Di3QAC07qGRvMF30IZ',
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "authorization: Basic QVJKYzRIZ2ZNY1ZUSDRaOGZ2M1hXN0sybEV6VjNLRkdlX2Q2cWxaX29SbDE3bGpTaEpBaVBxejE3VnVPM1hzbmh2VU54bmhqaUJpU3VlNXY6RUh6TGt5SGNTX1pvNlo2SGFyU2xPMWlGR0pfeUl1UXNpQmFpaHJVSFh0YVUwa25HMm1kZjM5ejFTQXVpeDNEaTNRQUMwN3FHUnZNRjMwSVo=",
      "cache-control: no-cache",
      "postman-token: ff081827-61f8-d49d-fbd0-c8092009167d"
    ),
  ));

  $response = curl_exec($curl);
  $jsonResponse = json_decode($response);

  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    return $jsonResponse->access_token;
  }
}
public function getEmail($access_token){


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.sandbox.paypal.com/v1/identity/openidconnect/userinfo/?schema=openid",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "authorization_code=A21AAFKUgqarKNNkidrQPrGNAePO-jQHmkH6q7S2uX8eUiPy2n0y-ZlUHZXMpSB2fbpByFKtc7oUJf5jjH8aKg_QK_CB5AqyA&grant_type=authorization_code3&=",
  CURLOPT_HTTPHEADER => array(
    "authorization: Bearer ".$access_token,
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 7c37b519-c147-f4dd-d65b-9fb2429e99f4"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
$jsonResponse = json_decode($response);
$email = $jsonResponse->email;
if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return $jsonResponse->email;
}
}

public function deductMoneyUsingMDID($userId, $ammount){
  // $user = User::find($userid);
  // $mdid = $user->mdid;
  $auth_code = 'C21AAER0-Y_nNR9A2UqD12MLc2Vg9sb9zCA_tQv18UYhsXvhdD1xEObT7MIDW62mgQYR8ShGhzr8ipfEG4L1KD5yOOBG_d-Pw';
  $mdid = 'ccf6417add9b4433bac3433690f3b773';
  $data = '{
             "intent":"sale",
             "payer":{
                "payment_method":"paypal"
             },
             "transactions":[
                {
                   "amount":{
                      "currency":"AUD",
                      "total":"'.$amount.'"
                   },
                   "description":"Trip payment"
                }
             ],
    "note_to_payer": "Contact us for any questions on your order.",
    "redirect_urls": {
    "return_url": "http://www.paypal.com/return",
    "cancel_url": "http://www.paypal.com/cancel"
    }
  }';
  $ch = curl_init('https://api.sandbox.paypal.com/v1/payments/payment');
  // curl_setopt($ch, CURLOPT_URL, "");
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Authorization: Bearer ".$auth_code,
    "PayPal-Client-Metadata-Id: ".$mdid)
    // "Content-length: ".strlen($data))
  );

  $result = curl_exec($ch);
  curl_close($ch);
  return "1".$result;
  // echo $data;


}
function PaypalFuturePayment($userID,$amount)
{
    $amount=number_format($amount,2);
    /* paypal App truxx dev client and secret key*/
    if($userID && $amount){
    $user = User::find($userID);

    $refresh_token = $user->auth_code;
    $Metadata_id = $user->mdid;
    // return $user;
    if($refresh_token && $Metadata_id){
        if ($_SERVER['SERVER_NAME'] == 'syonserver.com') {

            $clientId = "AT_XZO9-ih7GpqAcdOg5R9n_38Vkj6IiMXXfnv8cajIetz7LyxnzSleJw5BLbTS1ULVjNRk3ph0TIuUU";
            $secret = "EJ3DpGdgir2Ha5DnRvUwrNLsaGzYD-4lmFnki5GuvJekJTir0Ox58DUH1Sdx0gc-xX3GKsRvRt6pAHj3";

            $url1="https://api.sandbox.paypal.com/v1/oauth2/token";
            $url2="https://api.sandbox.paypal.com/v1/payments/payment";

        }else{
            $account = 0; // 0 for sandbox ,1 for live
            if ($account == 1) {
                //client live
                $clientId = "AT_XZO9-ih7GpqAcdOg5R9n_38Vkj6IiMXXfnv8cajIetz7LyxnzSleJw5BLbTS1ULVjNRk3ph0TIuUU";
                $secret = "EJ3DpGdgir2Ha5DnRvUwrNLsaGzYD-4lmFnki5GuvJekJTir0Ox58DUH1Sdx0gc-xX3GKsRvRt6pAHj3";

                $url1 = "https://api.paypal.com/v1/oauth2/token";
                $url2 = "https://api.paypal.com/v1/payments/payment";
            } else {
                //client sandbox
                $clientId = "AT_XZO9-ih7GpqAcdOg5R9n_38Vkj6IiMXXfnv8cajIetz7LyxnzSleJw5BLbTS1ULVjNRk3ph0TIuUU";
                $secret = "EJ3DpGdgir2Ha5DnRvUwrNLsaGzYD-4lmFnki5GuvJekJTir0Ox58DUH1Sdx0gc-xX3GKsRvRt6pAHj3";
                $url1 = "https://api.sandbox.paypal.com/v1/oauth2/token?grant_type=authorization_code&response_type=token&refresh_token=" . $this->getToken();
                $url2 = "https://api.sandbox.paypal.com/v1/payments/payment";
        }
    }


//
// //print_r($refresh_token);die;
//
//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_URL, $url1);
//         curl_setopt($ch, CURLOPT_HEADER, "Content-Type: application/x-www-form-urlencoded");
//         curl_setopt($ch, CURLOPT_USERPWD, $clientId . ":" . $secret);
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=" . $this->getToken());
//         $result = curl_exec($ch);
//         curl_close($ch);
//         $result=json_decode($result);
//         return $result;
//        //11111111   for payment Authorize: For example, to first authorize the payment, use a request similar to this:
//       //  return $result;
        // $access_token =  $result->access_token;
        $access_token = $this->getToken();
        $data = array(
            "intent" => "authorize",
            "payer" => array(
                "payment_method" => "paypal"
            ),
            "transactions" => array(
                array("amount" => array(
                    "currency" => "AUD",
                    "total" => $amount
                ),
                    "description" => "Buckle Up")
            ),
            "redirect_urls"=>array(
            "return_url"=> "http://www.paypal.com/return",
            "cancel_url"=> "http://www.paypal.com/cancel"
          ));

        $data_string = json_encode($data);
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $url2);
        $headers = array(
            'Content-Type: application/json',
            'PayPal-Client-Metadata-Id: '.$Metadata_id,
            'Authorization: Bearer '.$access_token,
            'Content-Length: ' . strlen($data_string)
        );
        curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch1, CURLOPT_POST, true);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_POSTFIELDS, $data_string);
        $result1 = curl_exec($ch1);
        curl_close($ch1);

        $result1=json_decode($result1);
        return $result1;
        $message  = $result1->message;

       if($result1->state=='approved'){
           $access_id=  $result1->transactions[0]->related_resources[0]->authorization->id;

       }else{
           if(empty($message)){
               $message ='Authorization error, Please try again.';
           }
           return array('response' => '', 'success' => '0','message'=>$message);
       }

       // print_r($result1);die;

      //2222222   capture the payment:
        $data =  array("amount" => array(
            "currency" => "USD",
            "total" => $amount
        ),
            "is_final_capture" => "true"
        );
        $data_string = json_encode($data);
        $ch2 = curl_init();
        if ($_SERVER['SERVER_NAME'] == 'syonserver.com') {
            curl_setopt($ch2, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/payments/authorization/$access_id/capture");
        }else {
            $account = 0; // 0 for sandbox ,1 for live
            if ($account == 1) {
                //client live
             curl_setopt($ch2, CURLOPT_URL, "https://api.paypal.com/v1/payments/authorization/$access_id/capture");
        }else{
                curl_setopt($ch2, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/payments/authorization/$access_id/capture");
            }
        }

        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer '.getToken(),
            'Content-Length: ' . strlen($data_string)
        );
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $data_string);
        $response = curl_exec($ch2);
        curl_close($ch2);

        $response_a = json_decode($response, true);
        $state = $response_a['state'];
        $message = $response_a['message'];
        return $response_a;
        // exit(1);
        if(!empty($response_a)){
            if($state=='completed') {
                return array('response' => $response_a, 'success' => '1','message'=>'Data received');
            }else{
                if(empty($message)){
                    $message ='Payment authorization error, Please try again.';
                }
                return array('response' => '', 'success' => '0','message'=>$message);
             }
        }
       else{
           return array('response' => '','success'=>'0','message'=>'Response nil');
           }
        }
      else
        {
            return array('response' => '', 'success' => '0','message'=>'Authorization code not available.');
        }
}else{

        return array('response' => '', 'success' => '0','message'=>'Unauthorize request.');

    }
}
public function getToken(){
    $clientId = 'AT_XZO9-ih7GpqAcdOg5R9n_38Vkj6IiMXXfnv8cajIetz7LyxnzSleJw5BLbTS1ULVjNRk3ph0TIuUU';
    $clientSecret = 'EJ3DpGdgir2Ha5DnRvUwrNLsaGzYD-4lmFnki5GuvJekJTir0Ox58DUH1Sdx0gc-xX3GKsRvRt6pAHj3';

$sdkConfig = array(
    "mode" => "sandbox"
);

$cred = new OAuthTokenCredential(
    $clientId,
    $clientSecret
);

$access_token = $cred->getAccessToken($sdkConfig);

return $access_token;
}


}


?>
