<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1><?php echo e(trans('quickadmin::qa.menus-createCustom-create_new_custom_controller')); ?></h1>

            <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php echo implode('', $errors->all('
                        <li class="error">:message</li>
                        ')); ?>

                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php echo Form::open(['class' => 'form-horizontal']); ?>


    <div class="form-group">
        <?php echo Form::label('parent_id', trans('quickadmin::qa.menus-createCustom-menu_parent') , ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php echo Form::select('parent_id', $parentsSelect, old('parent_id'), ['class'=>'form-control']); ?>

        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('name', trans('quickadmin::qa.menus-createCustom-controller_name'), ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php echo Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::qa.menus-createCustom-controller_name_placeholder')]); ?>

        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('title', trans('quickadmin::qa.menus-createCustom-menu_title'), ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php echo Form::text('title', old('title'), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::qa.menus-createCustom-menu_title_placeholder')]); ?>

        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('roles', trans('quickadmin::qa.menus-createCustom-roles'), ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div>
                    <label>
                        <?php echo Form::checkbox('roles['.$role->id.']',$role->id,old('roles.'.$role->id)); ?>

                        <?php echo $role->title; ?>

                    </label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('icon', trans('quickadmin::qa.menus-createCustom-icon'), ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php echo Form::text('icon', old('icon','fa-database'), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::qa.menus-createCustom-icon_placeholder')]); ?>

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?php echo Form::submit(trans('quickadmin::qa.menus-createCustom-create_controller'), ['class' => 'btn btn-primary']); ?>

        </div>
    </div>

    <?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>