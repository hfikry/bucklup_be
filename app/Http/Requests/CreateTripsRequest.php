<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTripsRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'user_poster' => 'required', 
            'user_joiner' => 'required', 
            'status' => 'required', 
            'trip_from' => 'required', 
            'trip_to' => 'required', 
            'seats' => 'required', 
            'price_per_seat' => 'required', 
            'date_time' => 'required', 
            
		];
	}
}
